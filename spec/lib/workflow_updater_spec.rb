# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/workflow_updater'

RSpec.describe WorkflowUpdater do
  subject { workflow_updater.execute }

  let(:workflow_updater) { WorkflowUpdater.new(project, token) }
  let(:project) { 5 }
  let(:token) { '123' }
  let(:labels) { ['workflow::in dev'] }
  let(:description) { 'Foo bar <!--tracking_issues_start 1,2 tracking_issues_end-->' }
  let(:mrs_json) do
    [
      {
        id: 199,
        labels: labels,
        description: description
      }
    ].to_json
  end
  let(:issue_json) do
    {
      labels: ['original label']
    }.to_json
  end

  before do
    stub_request(:get, 'https://gitlab.com/api/v4/projects/5/merge_requests?scope=assigned_to_me&per_page=10').
    with(headers: { 'Private-Token'=>'123' }).to_return(status: 200, body: mrs_json, headers: {})

    stub_request(:get, 'https://gitlab.com/api/v4/projects/5/issues/1').to_return(status: 200, body: issue_json, headers: {})
    stub_request(:get, 'https://gitlab.com/api/v4/projects/5/issues/2').to_return(status: 200, body: issue_json, headers: {})

    stub_request(:put, 'https://gitlab.com/api/v4/projects/5/issues/1').to_return(status: 200, body: '', headers: {})
    stub_request(:put, 'https://gitlab.com/api/v4/projects/5/issues/2').to_return(status: 200, body: '', headers: {})
  end

  it 'fetches labels from an MR and adds them to the tracking issues' do
    subject

    expect(WebMock).to have_requested(:get, 'https://gitlab.com/api/v4/projects/5/merge_requests?scope=assigned_to_me&per_page=10')

    expect(WebMock).to have_requested(:put, 'https://gitlab.com/api/v4/projects/5/issues/1').with(body: {'labels'=>['original label', 'workflow::in dev']})
    expect(WebMock).to have_requested(:put, 'https://gitlab.com/api/v4/projects/5/issues/2').with(body: {'labels'=>['original label', 'workflow::in dev']})
  end

  it 'writes logs' do
    expect(workflow_updater).to receive(:warn).with("Using project ##{project}")
    expect(workflow_updater).to receive(:warn).with("Updating tracker issues (1,2) for MR #199")

    subject
  end

  context 'when labels contain workflow and non workflow labels' do
    let(:labels) { ['workflow::in dev', 'foo'] }

    it 'only mirrors workflow labels' do
      subject

      expect(WebMock).to have_requested(:put, 'https://gitlab.com/api/v4/projects/5/issues/1').with(body: {'labels'=>['original label', 'workflow::in dev']})
    end
  end

  context 'when MR description does not reference a tracker issue' do
    let(:description) { '' }

    specify do
      expect(WebMock).not_to have_requested(:put, 'https://gitlab.com/api/v4/projects/5/issues/1')
      expect{ subject }.to_not raise_error
    end
  end
end
