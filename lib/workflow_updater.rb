# frozen_string_literal: true

require 'rubygems'
require 'bundler/setup'
require 'gitlab'

class WorkflowUpdater
  TRACKER_ISSUES_START = '<!--tracking_issues_start '
  TRACKER_ISSUES_END = ' tracking_issues_end-->'
  MRS_LIMIT = 10
  ALLOWED_ISSUES_REGEX = /workflow::.*/

  def initialize(project, token)
    @project = project
    @token = token
  end

  def execute
    Gitlab.endpoint = 'https://gitlab.com/api/v4'
    Gitlab.private_token = @token

    warn "Using project ##{@project}"

    Gitlab.merge_requests(@project, scope: 'assigned_to_me', per_page: MRS_LIMIT).each do |mr|
      tracker_issue_ids = mr.description[/#{TRACKER_ISSUES_START}(.*?)#{TRACKER_ISSUES_END}/m, 1]

      next if tracker_issue_ids.nil?

      warn "Updating tracker issues (#{tracker_issue_ids}) for MR ##{mr.id}"

      labels = mr.labels.select { |label| ALLOWED_ISSUES_REGEX.match(label) }

      tracker_issue_ids.split(',').each do |issue_id|
        original_labels = Gitlab.issue(@project, issue_id).labels
        Gitlab.edit_issue(@project, issue_id, { labels: original_labels + labels })
      end
    end
  end
end
