# GitLab workflow updater

Keeps workflow labels of merge requests in sync with related issues.

## Getting started

1. Fork this repository on GitLab.
1. In your fork, go to `https://gitlab.com/PATH_TO_PROJECT/-/pipeline_schedules`.
1. Click **New schedule**.
1. Enter a name e.g. `Update workflow labels`.
1. Choose an **Interval Pattern** e.g. `0 * * * *` (once per hour).
1. Add a variable `GITLAB_PROJECT` containing the project ID of the project that should be updated e.g. `29461287` for `gitlab-org/gitlab`.
1. Add a variable `GITLAB_TOKEN` containing your personal access token.
1. In a merge request description add `<!--tracking_issues_start 123 tracking_issues_end-->` with `123` being the ID of an issue.
1. The workflow label of the MR should be mirrored to the issue updating once per hour.

You can add multiple issues by adding comma separated IDs like `<!--tracking_issues_start 123,456,789 tracking_issues_end-->`
